const express = require('express')
const app = express()
const port = 3000
const webRouter = require("./routes/webRouter.js")

app.use(
    express.urlencoded({
        extended: false
    })
)
app.set("view engine", "ejs")
app.use(express.static("public")) 

app.use(webRouter) 

app.listen(port, () => {
    console.log("Server is running at http://localhost:"+port)
})